#include <systemc.h>
#include "left_shift_6.hpp"

using namespace std;

void left_shift_6::shift()
{
   sc_uint<6> concat;
   sc_uint<16> dato;
   concat=0;
   while(true)
   {
     wait(); 
     dato=(ingresso->read(),concat);
     uscita->write(dato);
   }

}
