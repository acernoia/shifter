#include <systemc.h>
#include <string>
#include "left_shift_6.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<10> >  input;
    sc_signal<sc_uint<16> > output;
    left_shift_6 LS;

    SC_CTOR(TestBench) : LS("LS")
    {
        SC_THREAD(stimulus_thread);
        LS.ingresso(this->input);
        LS.uscita(this->output);
        init_values();
    }
   
    int check()
    {
      for (unsigned i=0;i<TEST_SIZE;i++) 
      {
        if (dato_letto[i]!=res_test[i])
        {
	    cout << "TEST FALLITO: " << i << endl;
            cout << "RISULTATO TEORICO: " << res_test[i] << endl;
            cout << "RISULTATO TEST :" << dato_letto[i] << endl;
            return 1;
        }
      }
      cout << "TEST OK" << endl;
      return 0;
    }

    private:

    void stimulus_thread() 
    {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
           input.write(in_test[i]);
           wait(1,SC_NS);
           cout << "Ingresso : " << "      ";
           cout << input.read().to_string(SC_BIN,false);
           cout << " (" << input.read() << ")" << endl;
           dato_letto[i]=output.read();
           cout << "Uscita   : " << output.read().to_string(SC_BIN,false);
           cout << " (" << output.read() << ")" << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 8;
    unsigned short in_test[TEST_SIZE];
    unsigned short dato_letto[TEST_SIZE];
    unsigned short res_test[TEST_SIZE];
    void init_values()
    { 
      in_test[0] = 0;
      in_test[1] = 1;
      in_test[2] = 23;
      in_test[3] = 32;
      in_test[4] = 64;
      in_test[5] = 65;
      in_test[6] = 66;
      in_test[7] = 100;
      for (unsigned i=0;i<TEST_SIZE;i++) 
      {
          //Lo shift di 6 posizioni equivale a moltiplicare per 2^6=64
          res_test[i]=in_test[i]*64;
      }
    }
};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START TEST" << endl << endl;

  sc_start();

  return test.check();
} 
