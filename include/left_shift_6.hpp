#ifndef LEFT_SHIFT_6_HPP
#define LEFT_SHIFT_6_HPP

SC_MODULE(left_shift_6)
{
   sc_in<sc_uint<10> > ingresso;
   sc_out<sc_uint<16> >  uscita;

   SC_CTOR(left_shift_6)
   {
     SC_THREAD(shift);
     sensitive << ingresso;
   }
   private:
   void shift ();
};


#endif
